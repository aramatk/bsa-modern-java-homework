package com.binary_studio.fleet_commander.core.common;

public final class PositiveInteger {

	private final Integer underlyingVal;

	public PositiveInteger(Integer val) {
		if (val < 0) {
			throw new IllegalArgumentException(String.format("Got negative value %d, expected positive integer", val));
		}
		this.underlyingVal = val;
	}

	public static PositiveInteger of(Integer val) {
		return new PositiveInteger(val);
	}

	public Integer value() {
		return this.underlyingVal;
	}

	public boolean isZero() {
		return this.value() == 0;
	}

	public boolean gr(PositiveInteger val) {
		return this.value() > val.value();
	}

	public boolean goe(PositiveInteger val) {
		return this.value() >= val.value();
	}

	public boolean lt(PositiveInteger val) {
		return this.value() < val.value();
	}

	public boolean loe(PositiveInteger val) {
		return this.value() <= val.value();
	}

	public PositiveInteger add(PositiveInteger val) {
		return PositiveInteger.of(this.value() + val.value());
	}

	public PositiveInteger subtract(PositiveInteger val) {
		int result = this.value() - val.value();
		result = result > 0 ? result : 0;
		return PositiveInteger.of(result);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != PositiveInteger.class) {
			return false;
		}
		return ((PositiveInteger) obj).underlyingVal == this.underlyingVal;
	}

	@Override
	public int hashCode() {
		return this.underlyingVal.hashCode();
	}

}

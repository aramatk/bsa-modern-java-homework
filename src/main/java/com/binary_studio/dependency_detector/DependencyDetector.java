package com.binary_studio.dependency_detector;

import java.util.*;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		Map<String, List<String>> linkedHashMap = new HashMap<>();
		for (String library : libraries.libraries) {
			linkedHashMap.put(library, new ArrayList<>());
		}
		for (String[] dependency : libraries.dependencies) {
			linkedHashMap.get(dependency[0]).add(dependency[1]);
		}
		Set<String> visited = new HashSet<>();
		Set<String> recStack = new HashSet<>();
		for (String library : linkedHashMap.keySet()) {
			if (isCyclic(library, visited, recStack, linkedHashMap)) {
				return false;
			}
		}
		return true;
	}

	private static boolean isCyclic(String library, Set<String> visited, Set<String> recStack,
			Map<String, List<String>> dependencyMap) {
		if (recStack.contains(library)) {
			return true;
		}
		if (visited.contains(library)) {
			return false;
		}
		visited.add(library);
		recStack.add(library);

		List<String> children = dependencyMap.get(library);
		for (String c : children) {
			if (isCyclic(c, visited, recStack, dependencyMap)) {
				return true;
			}
		}
		recStack.remove(library);
		return false;
	}

}

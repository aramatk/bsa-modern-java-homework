package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		List<Integer> diff = new ArrayList<>();
		Iterator<Integer> iterator = prices.iterator();
		Integer prev = iterator.hasNext() ? iterator.next() : null;
		if (prev == null) {
			return 0;
		}
		while (iterator.hasNext()) {
			Integer current = iterator.next();
			diff.add(current - prev);
			prev = current;
		}
		return diff.stream().reduce(0, (profit, i) -> i > 0 ? profit + i : profit);
	}

}

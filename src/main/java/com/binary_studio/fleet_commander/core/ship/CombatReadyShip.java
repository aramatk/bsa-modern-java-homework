package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip ship;

	private PositiveInteger currentCapacitorAmount;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	public CombatReadyShip(DockedShip dockedShip) {
		this.ship = dockedShip;
		this.currentCapacitorAmount = dockedShip.getCapacitorAmount();
		this.currentShieldHP = dockedShip.getShieldHP();
		this.currentHullHP = dockedShip.getHullHP();
	}

	@Override
	public void endTurn() {
		this.currentCapacitorAmount = this.currentCapacitorAmount.add(this.ship.getCapacitorRechargeRate());
		if (this.currentCapacitorAmount.gr(this.ship.getCapacitorAmount())) {
			this.currentCapacitorAmount = this.ship.getCapacitorAmount();
		}
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.ship.getAttackSubsystem().getCapacitorConsumption().gr(this.currentCapacitorAmount)) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = this.currentCapacitorAmount
					.subtract(this.ship.getAttackSubsystem().getCapacitorConsumption());
			PositiveInteger damage = this.ship.getAttackSubsystem().attack(target);
			return Optional.of(new AttackAction(damage, this, target, this.ship.getAttackSubsystem()));
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction attackAction = this.ship.getDefenciveSubsystem().reduceDamage(attack);
		PositiveInteger damage = reduceCurrentShieldHP(attackAction.damage);
		this.currentHullHP = this.currentHullHP.subtract(damage);

		if (this.currentShieldHP.isZero() && this.currentHullHP.isZero()) {
			return new AttackResult.Destroyed();
		}
		else {
			return new AttackResult.DamageRecived(attackAction.weapon, attackAction.damage, attackAction.target);
		}
	}

	private PositiveInteger reduceCurrentShieldHP(PositiveInteger damage) {
		if (this.currentShieldHP.gr(PositiveInteger.of(0))) {
			if (this.currentShieldHP.gr(damage)) {
				this.currentShieldHP = this.currentShieldHP.subtract(damage);
				return PositiveInteger.of(0);
			}
			else {
				this.currentShieldHP = PositiveInteger.of(0);
				return damage.subtract(this.currentShieldHP);
			}
		}
		else {
			return damage;
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.currentCapacitorAmount.lt(this.ship.getDefenciveSubsystem().getCapacitorConsumption())) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = this.currentCapacitorAmount
					.subtract(this.ship.getDefenciveSubsystem().getCapacitorConsumption());
			RegenerateAction defenciveRegenerateAction = this.ship.getDefenciveSubsystem().regenerate();
			PositiveInteger newHullValue = regenerate(this.currentHullHP, defenciveRegenerateAction.hullHPRegenerated,
					this.ship.getHullHP());
			PositiveInteger newShieldValue = regenerate(this.currentShieldHP,
					defenciveRegenerateAction.shieldHPRegenerated, this.ship.getShieldHP());
			RegenerateAction resultRegenerateAction = new RegenerateAction(
					newShieldValue.subtract(this.currentShieldHP), newHullValue.subtract(this.currentHullHP));
			this.currentShieldHP = newShieldValue;
			this.currentHullHP = newHullValue;
			return Optional.of(resultRegenerateAction);
		}
	}

	private PositiveInteger regenerate(PositiveInteger currentValue, PositiveInteger defenciveRegValue,
			PositiveInteger shipValue) {
		var newValue = currentValue.add(defenciveRegValue);
		newValue = newValue.gr(shipValue) ? shipValue : newValue;
		return newValue;
	}

}
